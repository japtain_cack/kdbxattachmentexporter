# Keepass Database Attachment Exporter
## Usage
Using pykeepass for native kdbx support.

`pip3 install pykeepass`

`git clone https://gitlab.com/japtain_cack/kdbxattachmentexporter.git`

`cd kdbxattachmentexporter`

`./kdbxattachmentexporter.py database.kdbx`

All attachments will be exported under `./attachments`.

Filenames will be preserved if possible, otherwise the current epoch time
will be appended to the filename if there are duplicates for some reason.

